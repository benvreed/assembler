# Ben Reed
# Comp Org
# Program 1: No Disassemble!
# common.rb

# Declare all possible R-type operations
R_TYPES = [
  {op: :add, code: 0x0, funct: 0x20, pos: {rd: 1, rs:2, shamt: -1, rt: 3}},
  {op: :sub, code: 0x0, funct: 0x22, pos: {rd: 1, rs:2, shamt: -1, rt: 3}},
  {op: :and, code: 0x0, funct: 0x24, pos: {rd: 1, rs:2, shamt: -1, rt: 3}},
  {op: :or,  code: 0x0, funct: 0x25, pos: {rd: 1, rs:2, shamt: -1, rt: 3}},
  {op: :nor, code: 0x0, funct: 0x27, pos: {rd: 1, rs:2, shamt: -1, rt: 3}},
  {op: :slt, code: 0x0, funct: 0x2A, pos: {rd: 1, rs:2, shamt: -1, rt: 3}},
  {op: :sll, code: 0x0, funct: 0x00, pos: {rd: 1, rs:-1, shamt: 3, rt: 2}},
  {op: :srl, code: 0x0, funct: 0x02, pos: {rd: 1, rs:-1, shamt: 3, rt: 2}},
  {op: :jr,  code: 0x0, funct: 0x08, pos: {rd: -1, rs:1, shamt: -1, rt: -1}}
]

# Declare all possible I-type operations
I_TYPES = [
  {op: :addi, code: 0x8,  funct: 0x00, args: 3, branches: false},
  {op: :andi, code: 0xC,  funct: 0x00, args: 3, branches: false},
  {op: :ori,  code: 0xD,  funct: 0x00, args: 3, branches: false},
  {op: :beq,  code: 0x4,  funct: 0x00, args: 3, branches: true},
  {op: :bne,  code: 0x5,  funct: 0x00, args: 3, branches: true},
  {op: :lw,   code: 0x23, funct: 0x00, args: 2, branches: false},
  {op: :sw,   code: 0x2B, funct: 0x00, args: 2, branches: false}
]

# Declare all possible J-type operations
J_TYPES = [
  {op: :j,   code: 0x2,  funct: 0x00},
  {op: :jal, code: 0x3,  funct: 0x00}
]

# Declare all registers
$registers = {
  "$zero" => 0, "$at" => 1, "$v0" => 2, "$v1" => 3, "$a0" => 4, "$a1" => 5,
  "$a2" => 6, "$a3" => 7, "$t0" => 8, "$t1" => 9, "$t2" => 10, "$t3" => 11,
  "$t4" => 12, "$t5" => 13, "$t6" => 14, "$t7" => 15, "$s0" => 16, "$s1" => 17,
  "$s2" => 18, "$s3" => 19, "$s4" => 20, "$s5" => 21, "$s6" => 22, "$s7" => 23,
  "$t8" => 24, "$t9" => 25, "$k0" => 26, "$k1" => 27, "$gp" => 28, "$sp" => 29,
  "$fp" => 30, "$ra" => 31
}

def find_compliment(binary_str)
  # find 2's compliment string
  binary_str = binary_str.gsub(/[0]/, '?')
  binary_str = binary_str.gsub(/[1]/, '0')
  binary_str = binary_str.gsub(/[\?]/, '1')
  binary_str = binary_str.to_i(2)
  binary_str += 1
  "0#{binary_str.to_s(2)}"
end
