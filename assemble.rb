# Ben Reed
# Comp Org
# Program 1: No Disassemble!
# assemble.rb
require "./common"

$symbols = {}

def is_number?(str)
  return true if str == ""
  # return true if string is a number; otherwise, false
  # checks if the index of a regex match exists, if it does, it must be a number
  not (/\A[0-9]+\z/ =~ str).nil?
end

def get_hex_for_register(register)
  # remove commas and whitespace from register
  register_index = register.to_s().gsub(/[(\s)*(\,)(\s)*]/,'')

  # if it already is a number, just use this
  return register_index if is_number? register_index

  # ensure the register is known
  if $registers[register_index].nil?
    puts "Error! -- The #{register_index} does not exist in MIPS"
  end

  # return the number for the register
  $registers[register_index]
end

def get_immediate_and_register(argument)
  matches = argument.match /\A([0-9]+)\((.)*\)\z/
  rs = get_hex_for_register matches[1]
  immediate = matches[2]
  if is_number?(immediate) == false
    puts "Error! -- An offset must be a number"
  end
  return rs, immediate
end

def get_register_or_zeros(components, operation, type, index)
  # Return the binary representation based on the current operation, type of register
  # return 0 if the value is unused
  return 0x00 if index == -1

  if components[index]
    register_integer = get_hex_for_register components[index]
    return register_integer
  else
    puts "Error! -- Missing a required argument #{type} for #{operation[:op]}"
    return 0x00
  end
end

def printout_r_type(current, rd, rs, rt, shamt)
  puts "#{'%06b' % current[:code]} #{'%05b' % rs} #{'%05b' % rt} #{'%05b' % rd} #{'%05b' % shamt} #{'%06b' % current[:funct]}"
end

def printout_i_type(current, rs, rt, immediate)
  puts "#{'%06b' % current[:code]} #{'%05b' % rs} #{'%05b' % rt} #{'%016b' % immediate}"
end

def build_symbol_table(line)
  # build up the symbol table when labels are found
  $pc += 4

  matches = /\A\s*([A-Za-z\_]+)\:\s*((.)*)\n\z/.match line
  if not matches.nil?
    $symbols[matches[1]] = $pc
    line = matches[2]
  end
end

def abort_unknown_label!(label)
  if label.nil? || $symbols[label].nil?
    puts "Error! -- The label named #{label} was not found in your assembly"
    abort
  end
end

def find_label_offset(label, size)
# find offset using absolute value
  offset = ($pc - $symbols[label]).abs / 4
  
  # set 2's compliment
  if $pc > $symbols[label]
    offset = find_compliment(offset.to_s(2)).rjust(size, '1')
  else
    offset = "%0#{size}b" % offset
  end

  "#{offset}\n"
end

def parse(line)
  line_parts = line.split(" ")
  input_op = line_parts[0]

  $pc += 4
  
  R_TYPES.each do |current|
    # move on to the next iteration if the current operation does not match the input
    next if input_op != current[:op].to_s

    # find a numerical representation for each register or move on to the next one
    rd = get_register_or_zeros line_parts, current, :rd, current[:pos][:rd]
    rs = get_register_or_zeros line_parts, current, :rs, current[:pos][:rs]
    rt = get_register_or_zeros line_parts, current, :rt, current[:pos][:rt]
    shamt = get_register_or_zeros line_parts, current, :shamt, current[:pos][:shamt]

    # abort loop and print
    printout_r_type current, rd, rs, rt, shamt
    return
  end

  I_TYPES.each do |current|
    # move on to the next iteration if the current operation does not match the input
    next if input_op != current[:op].to_s

    if current[:args] == 3
      # find a numerical representation for each register or move on to the next one
      rt = get_register_or_zeros line_parts, current, :rt, 1
      rs = get_register_or_zeros line_parts, current, :rs, 2

      # if it branches, calculate label offset instead
      if current[:branches]
        label = line_parts[3]

        # abort if label is unknown; otherwise, set immediate
        abort_unknown_label! label
        immediate = find_label_offset(label, 16).to_i(2)
      else
        immediate = line_parts[3]
      end

      if immediate.nil?
        puts "Error! -- You need an immediate value for an immediate operation"
        return
      end
    else
      rt = get_register_or_zeros current, :rt, 1
      rs, immediate = get_immediate_and_register line_parts[2]
    end

    # abort loop and print
    printout_i_type current, rs, rt, immediate.to_i
    return
  end

  J_TYPES.each do |current|
    if input_op == current[:op].to_s
      label = line_parts[1]

      # abort if label not found, print otherwise
      abort_unknown_label! label
      print "#{"%06b" % current[:code]} "
      print find_label_offset label, 26
    end
  end
end

# set program counter
if ARGV[0] == "-initpc"
  ARGV.shift
  $pc_reset = ARGV[0].to_i(16)
  ARGV.shift
else
  $pc_reset = 0x00
end

$pc = $pc_reset

# run through it once to populate symbol table
File.foreach(ARGV[0]) { |line| build_symbol_table line }

$pc = $pc_reset

# then parse
File.foreach(ARGV[0]) { |line| parse line }
