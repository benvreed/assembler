# ASSEMBLER

Submitted by: Ben Reed  
Course: Comp Org  
Program Name:  No Disassemble!

## Prerequisites

Requires Ruby 2.0 or newer  
This is bundled with OS X Yosemite and Mavericks  
Older versions will need an installation from ruby-lang.org

## Usage

Since it is 2 very mimimal command line tools, I did not detail instructions to install them and add it to your PATH.  I kept it quick and painless. 

To run:

1. open terminal
2. navigate to this directory
3. type `ruby assemble.rb -initpc 0x00 [INSERT ASSEMBLY FILE NAME HERE] > [OUTPUT FILE]` to use the assembler
4. type `ruby disassemble.rb -initpc 0x00 [INSERT BINARY FILE HERE] > [OUTPUT FILE]` to use the disassembler

The `-initpc` flag tells the assembler where to start the program counter.  This is always represented in hexidecimal.  Without this flag present, the assembler assigns the first operation as PC=0x0.

As a command line app, if you do not specify an [OUTPUT FILE] pipe, it will print to STDOUT!

> Spacing does not matter in the disassembler, since all spaces are removed :)
