# Ben Reed
# Comp Org
# Program 1: No Disassemble!
# disassemble.rb
require "./common"

def try_r_type(current_instruction)
  # try it as an R-type operation 
  # return false if no
  rs_start = 6
  rt_start = rs_start + 5
  rd_start = rt_start + 5
  shamt_start = rd_start + 5
  funct_start = shamt_start + 5

  ci_operator = current_instruction[0..rs_start-1].to_i(2)
  ci_funct = current_instruction[funct_start...current_instruction.length].to_i(2)


  # determine if the current possibility matches
  R_TYPES.each do | possiblity |
    if ci_operator == possiblity[:code] && ci_funct == possiblity[:funct]
      print "#{possiblity[:op]} "

      rd = current_instruction[rd_start...shamt_start]
      print "#{rd.to_i(2)}, "

      rs = current_instruction[rs_start...rt_start]
      print "#{rs.to_i(2)}, "

      rt = current_instruction[rt_start...rd_start]
      print "#{rt.to_i(2)}\n"

      return true
    end
  end

  return false
end

def try_i_type(current_instruction)
  # try it as an I-type operation 
  # return false if no
  rs_start = 6
  rt_start = rs_start + 5
  immediate_start = rt_start + 5

  ci_operator = current_instruction[0..rs_start-1].to_i(2)

  # determine if the current possibility matches
  I_TYPES.each do | possiblity |
    if ci_operator == possiblity[:code]
      print "#{possiblity[:op]} "

      if possiblity[:args] == 3
        rt = current_instruction[rt_start...immediate_start]
        print "#{rt.to_i(2)}, "

        rs = current_instruction[rs_start...rt_start]
        print "#{rs.to_i(2)}, "

        immediate = current_instruction[immediate_start...current_instruction.length]

         # account for 2's compliment
        if immediate[0] == "1"
          immediate = find_compliment(immediate).to_i(2) * -1
        else
          immediate = immediate.to_i(2)
        end

        print "#{immediate}\n"

      else
        rt = current_instruction[rt_start...immediate_start]
        print "#{rt.to_i(2)}, "

        immediate = current_instruction[immediate_start...current_instruction.length]

        # account for 2's compliment
        if immediate[0] == "1"
          immediate = find_compliment(immediate).to_i(2) * -1
        else
          immediate = immediate.to_i(2)
        end
        print "#{immediate.to_i(2)}("

        rs = current_instruction[rs_start...rt_start]
        print "#{rs.to_i(2)})\n"
      end

      return true
    end
  end

  return false
end

def try_j_type(current_instruction)
  # try it as an J-type operation 
  # return false if no
  addr_start = 6

  ci_operator = current_instruction[0..addr_start-1].to_i(2)

  # determine if the current possibility matches
  J_TYPES.each do | possiblity |
    if ci_operator == possiblity[:code]
      print "#{possiblity[:op]} "

      addr = current_instruction[addr_start...current_instruction.length]

      # account for 2's compliment
      if addr[0] == "1"
        offset = find_compliment(addr).to_i(2) * -1
      else
        offset = addr.to_i(2)
      end

      print "#{offset}\n"
      return true
    end
  end

  return false
end

def translate(line)
  $pc += 4
  chunk = line.gsub(/(\s|\n)*/, '')

  if chunk.length % 32 != 0
    puts "Error! -- The binary file is malformed with operations not in 32 bits"
    abort
  end

  current_instruction = ""

  for i in 0...chunk.length
    current_instruction += chunk[i]

    if (i+1) % 32 == 0
      # if it is the end of the instruction
      # disassemble it
      if try_r_type(current_instruction) == false
        if try_i_type(current_instruction) == false
          if try_j_type(current_instruction) == false
            puts "Error! -- Your binary is malformed… not R|I|J-type compliant"
            abort
          end
        end
      end

      # reset for next instruction
      current_instruction = ""
    end
  end
end

if ARGV[0] == "-initpc"
  ARGV.shift
  $pc = ARGV[0].to_i(16)
  ARGV.shift
else
  $pc = 0x00
end

translate File.read(ARGV[0]).to_s
